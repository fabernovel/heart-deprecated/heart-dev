#!/bin/bash
cd node_modules/@fabernovel
for MODULE in $(ls)
do
  cd $MODULE
  npm i
  cd ..
done
