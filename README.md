# Description

_Heart Dev_ is an easiest way to handle the code of every _Heart_ module in a single repository.

It leverages the use of [Git Submodules feature](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

## Installation

Download the repository with the submodules: `git clone git@gitlab.com:fabernovel/heart/heart-dev.git --recursive`.

Run `npm run build` to install and build all submodules dependencies.

To run a service locally with variables from the .env `set -o allexport; source .env; set +o allexport; npx heart /dareboost '{"url": "fabernovel.com"}'`

## Usage

Don't forget that each submodule is still an independent repository.
